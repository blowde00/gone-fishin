package com.galvanize.gonefishin.data;

import com.galvanize.gonefishin.entities.*;
import com.galvanize.gonefishin.repositories.*;
import com.galvanize.gonefishin.utils.terminal.Printer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class Seeder {

    private final Printer printer;

    // Put you repository fields here
    @Autowired
    BaitRepository baitRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    RodRepository rodRepository;

    @Autowired
    FishRepository fishRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TackleBoxRepository tackleBoxRepository;

    // Make sure to add them to constructor below
    // IntelliJ can help with this

    @Autowired
    public Seeder(Printer printer, BaitRepository baitRepository, LocationRepository locationRepository, RodRepository rodRepository, FishRepository fishRepository, PlayerRepository playerRepository) {
        this.printer = printer;
    }

    public void seed() {
        printer.inline().print("Seeding Data...");
        // Use your repositories here to seed your data
        List<Bait> baitList = new ArrayList<>();
        List<Location> locationList = new ArrayList<>();
        List<FishingRod> rodList = new ArrayList<>();
        List<Fish> fishies = new ArrayList<>();
        List<TackleBox> tackleBoxList = new ArrayList<>();

        baitList.add(new Bait("worm", true, 1));
        baitList.add(new Bait("jig", false, 1));
        baitList.add(new Bait("crawdad", true, 3));
        baitList.add(new Bait("minnow", true, 3));
        baitList.add(new Bait("spinner bait", false, 4));
        baitList.add(new Bait("crank bait", false, 4));
        baitList.add(new Bait("stinkbait", true, 4));
        baitList.add(new Bait("spoon", false, 5));
        baitList.add(new Bait("wax worm", false, 8));
        baitList.add(new Bait("guts", true, 10));

        baitRepository.saveAll(baitList);

        locationList.add(new Location("Matt's Wacky Pond", "Pond", true));
        locationList.add(new Location("Roy Lake", "Lake", true));
        locationList.add(new Location("Brandon's Roaring River", "River", true));
        locationList.add(new Location("Sea of Streitmatter", "Sea", false));
        locationList.add(new Location("Specific Ocean", "Ocean", false));

        locationRepository.saveAll(locationList);

        rodList.add(new FishingRod("Uncle David's Hand Me Down", 1, 0));
        rodList.add(new FishingRod("Wood's Wooden Handle", 2, 20));
        rodList.add(new FishingRod("Alesha's Catch Anything", 3, 30));
        rodList.add(new FishingRod("Streitmatter's Spinner Special", 4, 40));
        rodList.add(new FishingRod("The Lowdermilk Special", 5, 50));
        rodList.add(new FishingRod("Captain Ahab Special", 6, 100));

        rodRepository.saveAll(rodList);

        List<Bait> tier1 = Arrays.asList(
                baitRepository.findByType("worm").get(),
                baitRepository.findByType("jig").get()
        );

        List<Bait> tier2 = Arrays.asList(
                baitRepository.findByType("crawdad").get(),
                baitRepository.findByType("minnow").get()
        );

        List<Bait> tier3 = Arrays.asList(
                baitRepository.findByType("spinner bait").get(),
                baitRepository.findByType("crank bait").get(),
                baitRepository.findByType("stinkbait").get()
        );

        List<Bait> tier4 = Arrays.asList(
                baitRepository.findByType("spoon").get(),
                baitRepository.findByType("wax worm").get()
        );

        List<Bait> tier5 = Arrays.asList(
                baitRepository.findByType("guts").get()
        );

        List<Location> allLocations = Arrays.asList(
                locationRepository.findByType("Pond").get(),
                locationRepository.findByType("Lake").get(),
                locationRepository.findByType("River").get(),
                locationRepository.findByType("Sea").get(),
                locationRepository.findByType("Ocean").get()
        );

        List<Location> lakeRiverSeaOcean = Arrays.asList(
                locationRepository.findByType("Lake").get(),
                locationRepository.findByType("River").get(),
                locationRepository.findByType("Sea").get(),
                locationRepository.findByType("Ocean").get()
        );

        List<Location> riverSeaOcean = Arrays.asList(
                locationRepository.findByType("River").get(),
                locationRepository.findByType("Sea").get(),
                locationRepository.findByType("Ocean").get()
        );

        List<Location> seaOcean = Arrays.asList(
                locationRepository.findByType("Sea").get(),
                locationRepository.findByType("Ocean").get()
        );

        List<Location> ocean = Arrays.asList(
                locationRepository.findByType("Ocean").get()
        );

        String sardineImage =
                "       .\n" +
                        "      \":\"\n" +
                        "    ___:____     |\"\\/\"|\n" +
                        "  ,'        `.    \\  /\n" +
                        "  |  O        \\___/  |\n" +
                        "~^~^~^~^~^~^~^~^~^~^~^~^~";

        String jawsImage =
                " _________         .    .\n" +
                        "(..       \\_    ,  |\\  /|\n" +
                        " \\       O  \\  /|  \\ \\/ /\n" +
                        "  \\______    \\/ |   \\  / \n" +
                        "     vvvv\\    \\ |   /  |\n" +
                        "     \\^^^^  ==   \\_/   |\n" +
                        "      `\\_   ===    \\.  |\n" +
                        "      / /\\_   \\ /      |\n" +
                        "      |/   \\_  \\|      /\n" +
                        "             \\________/";

        String crappieImage =
                "       ,-.-,-,\n" +
                "     _/ / / /       /)\n" +
                "   ,'        `.   ,'')\n" +
                " _/(@) `.      `./ ,')\n" +
                "(____,`  \\:`-.   \\ ,')\n" +
                " (_      /::::}  / `.)\n" +
                "  \\    ,' :,-' ,)\\ `.)\n" +
                "   `.        ,')  `..)\n" +
                "     \\-....-'\\      \\)     \n" +
                "      `-`-`-`-`";

        String anchovyImage =
            "                                                        .\n" +
                    "            ___---\"\"\"\"\"\"\"\"\"\"\"\"\"\"---____                / |\n" +
                    "       _--\"\"   \\)))))))))))))))))))))))\"\"\"\"\"___       /  |\n" +
                    "    _-\" _       \\))))))_-\"|))))))))))))))))))))\"\"\"---' __|\n" +
                    " _-\"   / \\       |))))|   |)))))))))))))))))))))))))/--  |\n" +
                    "<___.  \\_/       |))))|   |))))))))))))))))))))))))<-    |\n" +
                    " \"-_             |))))|   |)))))))))))))))))))))))))\\--__|\n" +
                    "    \"-_         /)))))) -_|))))))))))))))))))))___---.   |\n" +
                    "       \"--__   /)))))))))))))))))))))))_____\"\"\"       \\  |\n" +
                    "            \"\"\"---______________---\"\"\"\"                \\ |\n" +
                    "                                                        '";

        String smallMouthBassImage =
                "                                   ____\n" +
                        "                               /\\|    ~~\\\n" +
                        "                             /'  |   ,-. `\\\n" +
                        "                            |       | X |  |\n" +
                        "                           _|________`-'   |X\n" +
                        "                         /'          ~~~~~~~~~,\n" +
                        "                       /'             ,_____,/_\n" +
                        "                    ,/'        ___,'~~         ;\n" +
                        "~~~~~~~~|~~~~~~~|---          /  X,~~~~~~~~~~~~,\n" +
                        "        |       |            |  XX'____________'\n" +
                        "        |       |           /' XXX|            ;\n" +
                        "        |       |        --x|  XXX,~~~~~~~~~~~~,\n" +
                        "        |       |          X|     '____________'\n" +
                        "        |   o   |---~~~~\\__XX\\             |XX\n" +
                        "        |       |          XXX`\\          /XXXX\n" +
                        "~~~~~~~~'~~~~~~~'               `\\xXXXXx/' \\XXX\n" +
                        "                                 /XXXXXX\\\n" +
                        "                               /XXXXXXXXXX\\\n" +
                        "                             /XXXXXX/^\\XXXXX\\\n" +
                        "                            ~~~~~~~~   ~~~~~~~";
        String carpImage =
                "       o                 o\n" +
                        "                  o\n" +
                        "         o   ______      o\n" +
                        "           _/  (   \\_\n" +
                        " _       _/  (       \\_  O\n" +
                        "| \\_   _/  (   (    0  \\\n" +
                        "|== \\_/  (   (          |\n" +
                        "|=== _ (   (   (        |\n" +
                        "|==_/ \\_ (   (          |\n" +
                        "|_/     \\_ (   (    \\__/\n" +
                        "          \\_ (      _/\n" +
                        "            |  |___/\n" +
                        "           /__/";

        String pufferfishImage =
                "                          .\n" +
                "                          A       ;\n" +
                "                |   ,--,-/ \\---,-/|  ,\n" +
                "               _|\\,'. /|      /|   `/|-.\n" +
                "           \\`.'    /|      ,            `;.\n" +
                "          ,'\\   A     A         A   A _ /| `.;\n" +
                "        ,/  _              A       _  / _   /|  ;\n" +
                "       /\\  / \\   ,  ,           A  /    /     `/|\n" +
                "      /_| | _ \\         ,     ,             ,/  \\\n" +
                "     // | |/ `.\\  ,-      ,       ,   ,/ ,/      \\/\n" +
                "     / @| |@  / /'   \\  \\      ,              >  /|    ,--.\n" +
                "    |\\_/   \\_/ /      |  |           ,  ,/        \\  ./' __:..\n" +
                "    |  __ __  |       |  | .--.  ,         >  >   |-'   /     `\n" +
                "  ,/| /  '  \\ |       |  |     \\      ,           |    /\n" +
                " /  |<--.__,->|       |  | .    `.        >  >    /   (\n" +
                "/_,' \\\\  ^  /  \\     /  /   `.    >--            /^\\   |\n" +
                "      \\\\___/    \\   /  /      \\__'     \\   \\   \\/   \\  |\n" +
                "       `.   |/          ,  ,                  /`\\    \\  )\n" +
                "         \\  '  |/    ,       V    \\          /        `-\\\n" +
                "          `|/  '  V      V           \\    \\.'            \\_\n" +
                "           '`-.       V       V        \\./'\\\n" +
                "               `|/-.      \\ /   \\ /,---`\\         kat\n" +
                "                /   `._____V_____V'\n" +
                "                           '     '";
        String bluefishImage = "/\\\n" +
                "      _/./\n" +
                "   ,-'    `-:.,-'/\n" +
                "  > O )<)    _  (\n" +
                "   `-._  _.:' `-.\\\n" +
                "       `` \\;";
        String largeMouthBassImage = " O\n" +
                "O\n" +
                " o  \\``/\n" +
                "    /o `))\n" +
                "   /_/\\_ss))\n" +
                "       |_ss))/|\n" +
                "      |__ss))_|\n" +
                "     |__sss))_|\n" +
                "     |___ss))\\|\n" +
                "      |_ss))\n" +
                "       )_s))\n" +
                " (`(  /_s))\n" +
                "  (_\\/_s))\n" +
                "   (\\/))";
        String walleyeImage = ".'.__\n" +
                "                      _.'      .\n" +
                "':'.               .''   __ __  .\n" +
                "  '.:._          ./  _ ''     \"-'.__\n" +
                ".'''-: \"\"\"-._    | .                \"-\"._\n" +
                " '.     .    \"._.'                       \"\n" +
                "    '.   \"-.___ .        .'          .  :o'.\n" +
                "      |   .----  .      .           .'     (\n" +
                "       '|  ----. '   ,.._                _-'\n" +
                "        .' .---  |.\"\"  .-:;.. _____.----'\n" +
                "        |   .-\"\"\"\"    |      '\n" +
                "      .'  _'         .'    _'\n" +
                "     |_.-'            '-.";
        String catfishImage =
                "           FISHKISSFISHKIS               \n" +
                "       SFISHKISSFISHKISSFISH            F\n" +
                "    ISHK   ISSFISHKISSFISHKISS         FI\n" +
                "  SHKISS   FISHKISSFISHKISSFISS       FIS\n" +
                "HKISSFISHKISSFISHKISSFISHKISSFISH    KISS\n" +
                "  FISHKISSFISHKISSFISHKISSFISHKISS  FISHK\n" +
                "      SSFISHKISSFISHKISSFISHKISSFISHKISSF\n" +
                "  ISHKISSFISHKISSFISHKISSFISHKISSF  ISHKI\n" +
                "SSFISHKISSFISHKISSFISHKISSFISHKIS    SFIS\n" +
                "  HKISSFISHKISSFISHKISSFISHKISS       FIS\n" +
                "    HKISSFISHKISSFISHKISSFISHK         IS\n" +
                "       SFISHKISSFISHKISSFISH            K\n" +
                "         ISSFISHKISSFISHK ";
        String tunaImage =
                "      _______\n" +
                " ,-~~~       ~~~-,\n" +
                "(                 )\n" +
                " \\_-, , , , , ,-_/\n" +
                "    / / | | \\ \\\n" +
                "    | | | | | |\n" +
                "    | | | | | |\n" +
                "   / / /   \\ \\ \\\n" +
                "   | | |   | | |";
        String bluegillImage = "o\n" +
                "o      ______/~/~/~/__           /((\n" +
                "  o  // __            ====__    /_((\n" +
                " o  //  @))       ))))      ===/__((\n" +
                "    ))           )))))))        __((\n" +
                "    \\\\     \\)     ))))    __===\\ _((\n" +
                "     \\\\_______________====      \\_((\n" +
                "                                 \\((";

        fishies.add(new Fish("Sardine", "Tiny", 2, 1, tier1, allLocations, sardineImage));
        fishies.add(new Fish("Bluegill", "Small", 2, 1, tier1, allLocations, bluegillImage));
        fishies.add(new Fish("Anchovy", "Small", 4, 2, tier2, allLocations, anchovyImage));
        fishies.add(new Fish("Smallmouth Bass", "Small", 10, 2, tier2, lakeRiverSeaOcean, smallMouthBassImage));
        fishies.add(new Fish("Crappie", "Medium", 4, 2, tier2, lakeRiverSeaOcean, crappieImage));
        fishies.add(new Fish("Largemouth Bass", "Medium", 6, 3, tier3, lakeRiverSeaOcean, largeMouthBassImage));
        fishies.add(new Fish("Walleye", "Medium", 6, 3, tier3, riverSeaOcean, walleyeImage));
        fishies.add(new Fish("Catfish", "Large", 8, 4, tier4, riverSeaOcean, catfishImage));
        fishies.add(new Fish("Tuna", "Large", 8, 4, tier4, seaOcean, tunaImage));
        fishies.add(new Fish("Carp", "X-Large", 8, 4, tier4, seaOcean, carpImage));
        fishies.add(new Fish("Pufferfish", "Medium", 10, 5, tier4, seaOcean, pufferfishImage));
        fishies.add(new Fish("Blue fish", "X-Large", 10, 5, tier4, ocean, bluefishImage));
        fishies.add(new Fish("Jaws", "Ginormous", 12, 6, tier5, ocean, jawsImage));

        fishRepository.saveAll(fishies);

        Player player = new Player(rodRepository.findByName("Uncle David's Hand Me Down").get(), baitRepository.findByType("worm").get(), locationRepository.findByName("Matt's Wacky Pond").get(), 5);
        playerRepository.save(player);

        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player1 = players.get(0);

        tackleBoxList.add(new TackleBox(baitRepository.findByType("worm").get(), 1, player1));
        tackleBoxRepository.saveAll(tackleBoxList);


        printer.print("Done.");
    }
}
