package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import com.galvanize.gonefishin.data.SettingsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class SettingsMenu extends Menu {

  // This is needed to map user selection of
  // a setting to the name of the setting
  private String[] nameArray;

  private final SettingsManager manager;

  // A line separator
  private final String separator = "-".repeat(20);

  @Autowired
  public SettingsMenu(Printer printer, Prompt prompt, SettingsManager manager) {
    super(printer, prompt, "Manage Settings",
      new String[] {
        "Change Setting"
      });

    this.manager = manager;
  }

  @Override
  public void load() {
    nameArray = manager.getSettingNames().toArray(String[]::new);

    printer.print(separator);

    printer.multiline(
      "<bold>Current Settings</bold>",
      separator
    )
    .print();

    displayCurrentValues();

    printer.print(separator);

    Integer choice = promptOptions();
    if(isExit(choice)) return;

    printer.inline().print("Which Setting");
    setSetting(
      nameArray[prompt.getChoice(nameArray.length) - 1]
    );

    load();
  }

  public void displayCurrentValues() {
    printer.formatAsList(
      Arrays.stream(nameArray)
        .map(n -> n + ": " + manager.getValue(n) + "\n" + separator)
        .toArray(String[]::new)
    );
  }

  private void setSetting(String name) {
    String newValue;
    int tries = 0;

    do {
      if(tries > 0) printer.print(Prompt.invalidInputMessage);

      printer.inline().print("Value");
      newValue = prompt.getString();
      tries++;
    } while(!manager.set(name, newValue));

    manager.save(name);
    printer.print("Setting Saved");
  }
}
