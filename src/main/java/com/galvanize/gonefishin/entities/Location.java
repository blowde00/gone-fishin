package com.galvanize.gonefishin.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private String type;
    private boolean isFreshwater;

    public Location() {}

    public Location(String name, String type, boolean isFreshwater) {
        this.name = name;
        this.type = type;
        this.isFreshwater = isFreshwater;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isFreshwater() {
        return isFreshwater;
    }

    public void setFreshwater(boolean freshwater) {
        isFreshwater = freshwater;
    }
}
