package com.galvanize.gonefishin.integration_tests;

import com.galvanize.gonefishin.repositories.LocationRepository;
import com.galvanize.gonefishin.entities.Bait;
import com.galvanize.gonefishin.entities.Fish;
import com.galvanize.gonefishin.entities.Location;
import com.galvanize.gonefishin.repositories.BaitRepository;
import com.galvanize.gonefishin.repositories.FishRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class FishIntTest {

    @Autowired
    FishRepository fishRepository;

    @Autowired
    BaitRepository baitRepository;

    @Autowired
    LocationRepository locationRepository;

    @Test
    public void canSaveFish() {
        List<Bait> baitList = Arrays.asList(
                new Bait("worm", true, 1),
                new Bait("wax worm", true, 2),
                new Bait("grub", true, 5)
        );

        List<Location> locationList = Arrays.asList(
                new Location("Matt's Wacky Pond", "Pond", true),
                new Location("Brandon's Roaring River", "River", true),
                new Location("Sea of Streitmatter", "Sea", false)
        );

        baitList.forEach(i -> assertNull(i.getId()));
        baitRepository.saveAll(baitList);
        baitList.forEach(i -> assertNotNull(i.getId()));

        locationList.forEach(j -> assertNull(j.getId()));
        locationRepository.saveAll(locationList);
        locationList.forEach(j -> assertNotNull(j.getId()));

        Fish fish = new Fish("Pufferfish", "Medium", 10, 5, baitList, locationList, "");
        assertNull(fish.getId());
        Fish result = fishRepository.save(fish);
        assertNotNull(fish.getId());

        assertEquals(fish.getSpecies(), result.getSpecies());

        Optional<Fish> actual = fishRepository.findById(result.getId());
        assertTrue(actual.isPresent());
        assertEquals(fish.getSpecies(), actual.get().getSpecies());
    }
}
