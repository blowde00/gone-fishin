package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/*
 * Base class for all menus. Provides common
 * logic and helpers for menu classes.
 */
public abstract class Menu {

  protected final Printer printer;
  protected final Prompt prompt;

  // Text to use when making this menu an option
  // in another menu
  private final String loadOption;

  // The texts of the options available in this menu
  // An exit option will be added automatically
  private final String[] options;

  public Menu(Printer printer, Prompt prompt, String loadOption, String[] options) {
    this.printer = printer;
    this.prompt = prompt;
    this.loadOption = loadOption;
    this.options = Stream.concat(
      Arrays.stream(options),
      Arrays.stream(new String[] { "Exit" })
    )
    .toArray(String[]::new);
  }

  // Every menu needs a load method
  public abstract void load();

  public String getLoadOption() {
    return loadOption;
  }

  /*
   * Helper that will display the options and
   * prompt for a selection
   */
  public Integer promptOptions() {
    printer.formatAsList(options).print();
    printer.inline().print("action");
    return prompt.getChoice(options.length);
  }

  /*
   * Helper to determine if the exit option was selected
   */
  public Boolean isExit(Integer choice) {
    return choice.equals(getExitChoice());
  }

  public String getOption(Integer index) {
    return options[index];
  }

  /*
   * Helper that can be used directly with the return
   * value of promptChoices()
   */
  public String getOptionByChoice(Integer promptChoice) {
    return getOption(promptChoice - 1);
  }

  /*
   * Helper that returns the value that can be
   * used to select the exit option
   */
  public Integer getExitChoice() {
    return options.length;
  }

  /*
   * Streams the List of Menu items to map to access #getLoadOption
   * and convert the final results to an array of Strings.
   */
  public static String[] menuListToOptionArray(List<Menu> menus) {
    return menus.stream()
      .map(Menu::getLoadOption)
      .toArray(String[]::new);
  }
}
