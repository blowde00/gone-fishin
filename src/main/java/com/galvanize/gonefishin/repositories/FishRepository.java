package com.galvanize.gonefishin.repositories;

import com.galvanize.gonefishin.entities.Fish;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FishRepository extends CrudRepository<Fish, Integer> {
    Optional<Fish> findBySpecies(String species);
    Optional<Fish> findByLocation(String location);
}
