package com.galvanize.gonefishin.integration_tests;

import com.galvanize.gonefishin.entities.Bait;
import com.galvanize.gonefishin.repositories.BaitRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class BaitIntTest {

    @Autowired
    BaitRepository baitRepository;

    @Test
    public void addBaitToRepo() {
        Bait bait = new Bait("worm", true, 1);
        assertNull(bait.getId());
        Bait result = baitRepository.save(bait);
        assertNotNull(bait.getId());

        assertEquals(bait.getType(), result.getType());

        Optional<Bait> actual = baitRepository.findById(result.getId());
        assertTrue(actual.isPresent());
        assertEquals(bait.getType(), actual.get().getType());

    }
}
