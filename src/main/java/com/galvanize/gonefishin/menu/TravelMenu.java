package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.entities.Bait;
import com.galvanize.gonefishin.entities.Fish;
import com.galvanize.gonefishin.entities.Location;
import com.galvanize.gonefishin.entities.Player;
import com.galvanize.gonefishin.repositories.FishRepository;
import com.galvanize.gonefishin.repositories.LocationRepository;
import com.galvanize.gonefishin.repositories.PlayerRepository;
import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TravelMenu extends Menu {

    private final String tableRowBorder = "+" + "-".repeat(66) + "+";

    public static String tableHeader = "<purple>|</purple> Fish            <purple>|</purple> Strength <purple>|</purple> Bait                                <purple>|</purple>";

    private final String separator = "-".repeat(16);

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private FishRepository fishRepository;

    @Autowired
    PlayerRepository playerRepository;

    public TravelMenu(Printer printer, Prompt prompt) {
        super(printer, prompt, "Fishin Locations", new String[]{
        });
    }

    @Override
    public void load() {
        Iterable<Location> locationIterable = locationRepository.findAll();
        List<Location> locationList = Streamable.of(locationIterable).toList();
        ArrayList<Location> locationArrayList = new ArrayList<>(locationList);

        printer.print("%n<bold cyan>Fishin Locations</bold cyan>");
        printer.print(separator);

        for (int i = 0; i < locationArrayList.size(); i++) {
            Location location = locationArrayList.get(i);

            printer.print(
                    "[%d] %s",
                    i + 1,
                    location.getName());
        }
        printer.print("[6] Exit");
        printer.inline().print("Which location");
        int action = prompt.getChoice(locationArrayList.size()+1);
        if (action == locationArrayList.size() + 1) {
            return;
        }
        Location location = locationArrayList.get(action - 1);
        printLocationSummary(location);
        printer.print("Travel to %s?", location.getName());
        if (!prompt.getYesNo()) {
            load();
        } else {
            travelToLocation(location);
            return;
        }
    }

    private void printLocationSummary(Location location) {
        Iterable<Fish> fishIterable = fishRepository.findAll();
        List<Fish> fishList = Streamable.of(fishIterable).toList();
        ArrayList<Fish> fishArrayList = new ArrayList<>(fishList);

        String waterType = "freshwater";
        if (!location.isFreshwater()) {
            waterType = "saltwater";
        }

        printer.multiline(
                "<blue underline>%n%s</blue underline>", "This is a %s location and contains the following fish: ").print(location.getName(), waterType);


        printer.multiline("<purple>%s</purple>", "%s", "<purple>%s</purple>").print(tableRowBorder, tableHeader, tableRowBorder);
        for (int i = 0; i < fishArrayList.size(); i++) {
            Fish fish = fishArrayList.get(i);

            List<Location> fishLocations = fish.getLocation();
            List<Bait> fishBait = fish.getBait();

            String bait1 = "";
            String bait2 = "";
            String bait3 = "";

            for (int j = 0; j < fishBait.size(); j++) {
                if (fishBait.size() == 1) {
                    bait1 = fishBait.get(0).getType();
                } else if (fishBait.size() == 2) {
                    bait1 = fishBait.get(0).getType();
                    bait2 = fishBait.get(1).getType();
                } else {
                    bait1 = fishBait.get(0).getType();
                    bait2 = fishBait.get(1).getType();
                    bait3 = fishBait.get(2).getType();
                }
            }
            for (int k = 0; k < fishLocations.size(); k++) {
                if (fishLocations.get(k).getName().equals(location.getName())) {
                    int fishLen = 16 - fish.getSpecies().length();
                    String space = " ";
                    int bait1Len = bait1.length();
                    int bait2Len = bait2.length() + 2;
                    int bait3Len = bait3.length() + 2;

                    int baitLen = 36;
                    if (fishBait.size() == 1) {
                        baitLen = baitLen - bait1Len;
                        printer.print("<purple>|</purple> %s%s<purple>|</purple>    %d     <purple>|</purple> %s%s<purple>|</purple>",
                                fish.getSpecies(),
                                space.repeat(fishLen),
                                fish.getFishStrength(),
                                bait1,
                                space.repeat(baitLen));
//                        printer.print(tableRowBorder);
                    } else if (fishBait.size() == 2) {
                        baitLen = baitLen - (bait1Len + bait2Len);
                        printer.print("<purple>|</purple> %s%s<purple>|</purple>    %d     <purple>|</purple> %s, %s%s<purple>|</purple>",
                                fish.getSpecies(),
                                space.repeat(fishLen),
                                fish.getFishStrength(),
                                bait1,
                                bait2,
                                space.repeat(baitLen));
//                        printer.print(tableRowBorder);
                    } else {
                        baitLen = baitLen - (bait1Len + bait2Len + bait3Len);
                        printer.print("<purple>|</purple> %s%s<purple>|</purple>    %d     <purple>|</purple> %s, %s, %s%s<purple>|</purple>",
                                fish.getSpecies(),
                                space.repeat(fishLen),
                                fish.getFishStrength(),
                                bait1,
                                bait2,
                                bait3,
                                space.repeat(baitLen));

                    }
                }

            }
        }
        printer.print("<purple>"+tableRowBorder+"</purple>");
    }

    private void travelToLocation(Location location) {
        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player1 = players.get(0);

        player1.setLocation(location);
        playerRepository.save(player1);
        printer.print("Welcome to %s! What would you like to do next?", player1.getLocation().getName());
    }
}
