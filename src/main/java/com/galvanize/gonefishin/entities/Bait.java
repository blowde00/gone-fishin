package com.galvanize.gonefishin.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Bait {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String type;
    private Boolean isReal;
    private Integer cost;

    public Bait(){}

    public Bait(String type, Boolean isReal, Integer cost) {
        this.type = type;
        this.isReal = isReal;
        this.cost = cost;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isReal() {
        return isReal;
    }

    public void setReal(Boolean real) {
        isReal = real;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
