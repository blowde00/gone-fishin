package com.galvanize.gonefishin.data;

import com.galvanize.gonefishin.data.entities.Setting;
import com.galvanize.gonefishin.data.repositories.SettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class SettingsManager {

  private final Map<String, Setting> settings = new HashMap<>();

  private final SettingRepository settingRepository;

  @Autowired
  public SettingsManager(List<Setting> settingsToManage, SettingRepository settingRepository) {

    // Store the given settings to manage in the settings map
    settingsToManage.forEach(s -> settings.put(s.getName(), s));

    this.settingRepository = settingRepository;
  }

  public void load() {

    // Get the saved settings from the database
    List<Setting> savedSettings = settingRepository.findAll();

    // Set any managed settings to their saved entry from the database
    savedSettings.forEach(s -> {
      Setting managedSetting = settings.getOrDefault(s.getName(), null);
      if(managedSetting != null) managedSetting.copyFrom(s);
    });

    // Save any managed settings that do not exist in the database
    settingRepository.saveAll(
      settings.values().stream()
        .filter(s -> !s.hasId())
        .collect(Collectors.toList())
    );
  }

  public List<String> getSettingNames() {
    return new ArrayList<>(settings.keySet());
  }

  public Setting getSetting(String name) {
    return settings.getOrDefault(name, null);
  }

  public String getValue(String name) {
    Setting setting = getSetting(name);

    if(setting != null) {
      return setting.getValue();
    }

    return null;
  }

  private Boolean canConvertSetting(Setting settingToCheck, SettingType targetType) {
    return (
      settingToCheck != null &&
      settingToCheck.isType(targetType) &&
      settingToCheck.getValue() != null &&
      !settingToCheck.getValue().isEmpty()
    );
  }

  public Boolean getBinary(String name) {
    Setting setting = getSetting(name);

    if(canConvertSetting(setting, SettingType.BINARY)) {
      String settingValue = setting.getValue().toLowerCase();
      if(settingValue.equals("true") || settingValue.equals("false"))
        return Boolean.valueOf(settingValue);
    }

    return null;
  }

  public Integer getInteger(String name) {
    Setting setting = getSetting(name);

    if(canConvertSetting(setting, SettingType.NUMBER)) {
      try {
        return Integer.valueOf(setting.getValue());
      } catch(NumberFormatException e) {
        return null;
      }
    }

    return null;
  }

  public Float getFloat(String name) {
    Setting setting = getSetting(name);

    if(canConvertSetting(setting, SettingType.NUMBER)) {
      try {
        return Float.valueOf(setting.getValue());
      } catch(NumberFormatException e) {
        return null;
      }
    }

    return null;
  }

  public Boolean set(String name, String newValue) {
    Setting setting = getSetting(name);

    if(setting == null) return false;

    String currentValue = setting.getValue();
    boolean revert = false;

    switch(setting.getType()) {
      case BINARY:
        setting.setValue(newValue);
        if(getBinary(name) == null) revert = true;
        break;
      case NUMBER:
        setting.setValue(newValue);
        if(newValue.contains(".")) {
          if(getFloat(name) == null) revert = true;
        } else {
          if(getInteger(name) == null)  revert = true;
        }
        break;
      default:
        setting.setValue(newValue);
    }

    if(revert) {
      setting.setValue(currentValue);
      return false;
    }

    return true;
  }

  public Boolean set(String name, Boolean newBinaryValue) {
    return set(name, newBinaryValue.toString());
  }

  public Boolean set(String name, Integer newIntegerValue) {
    return set(name, newIntegerValue.toString());
  }

  public Boolean set(String name, Float newFloatValue) {
    return set(name, newFloatValue.toString());
  }

  public void save(String name) {
    Setting setting = getSetting(name);
    if(setting != null) settingRepository.save(setting);
  }

  public void save() {
    settingRepository.saveAll(settings.values());
  }
}
