package com.galvanize.gonefishin.repositories;

import com.galvanize.gonefishin.entities.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {
    Optional<Location> findByName(String name);
    Optional<Location> findByType(String type);
}
