package com.galvanize.gonefishin.data;

import com.galvanize.gonefishin.utils.terminal.Printer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class SeederTest {

  @MockBean
  Printer printer;

  @Autowired
  Seeder seeder;

  // Autowire your repositories here

  @Test
  public void shouldSeedNeededData() {
    when(printer.inline()).thenReturn(printer);

    seeder.seed();

    // Use your repositories to verify
    // record counts and any important
    // data you need to seed
  }

}
