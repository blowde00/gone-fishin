package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import com.galvanize.gonefishin.GameConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MainMenu extends Menu {

  private final List<Menu> rootMenus;
  private Boolean firstRun = true;
  // A line separator
  private final String separator = "-".repeat(10);

  @Autowired
  public MainMenu(Printer printer, Prompt prompt, List<Menu> rootMenus) {
    super(printer, prompt, "Goto Main Menu",  menuListToOptionArray(rootMenus));

    this.rootMenus = rootMenus;
  }

  @Override
  public void load() {
    if(firstRun) {
      printer.print(GameConfiguration.welcomeMessaage);
      firstRun = false;
    }

    printer.print("%n<bold cyan>Main Menu</bold cyan>");
    printer.print(separator);
    Integer choice = promptOptions();
    if(!isExit(choice)) {
      this.rootMenus.get(choice - 1).load();
      load();
    }
  }
}
