package com.galvanize.gonefishin.integration_tests;

import com.galvanize.gonefishin.entities.*;
import com.galvanize.gonefishin.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PlayerIntTest {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    BaitRepository baitRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    RodRepository rodRepository;

    @Test
    public void addPlayerToRepo() {
        Bait bait = new Bait("worm", true, 1);
        Bait baitResult = baitRepository.save(bait);

        Location location = new Location("Matt's Wacky Pond", "Pond", true);
        Location locationResult = locationRepository.save(location);

        FishingRod fishingRod = new FishingRod("Uncle David's Hand Me Down", 1, 0);
        FishingRod fishingRodResult = rodRepository.save(fishingRod);

        Player player = new Player(fishingRodResult, baitResult, locationResult, 10);
        assertNull(player.getId());
        Player result = playerRepository.save(player);
        assertNotNull(player.getId());

        assertEquals(player.getBank(), result.getBank());

        Optional<Player> possiblePlayer = playerRepository.findById(result.getId());
        assertTrue(possiblePlayer.isPresent());
        Player foundPlayer = possiblePlayer.get();
        assertEquals(player.getBank(), foundPlayer.getBank());
    }

}
