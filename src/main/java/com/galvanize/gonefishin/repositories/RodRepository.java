package com.galvanize.gonefishin.repositories;

import com.galvanize.gonefishin.entities.FishingRod;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RodRepository extends CrudRepository<FishingRod, Integer> {
    Optional<FishingRod> findByName(String name);
}
