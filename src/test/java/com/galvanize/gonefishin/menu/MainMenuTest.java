package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.GameConfiguration;
import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class MainMenuTest {

  @MockBean
  Printer printer;

  @MockBean
  Prompt prompt;

  MainMenu mainMenu;
  List<Menu> submenus;
  Integer exitChoice;

  @BeforeEach
  public void setupTest() {
    // Create new mock submenus for each test
    submenus = new ArrayList<>(List.of(
      Mockito.mock(Menu.class),
      Mockito.mock(Menu.class),
      Mockito.mock(Menu.class)
    ));
    exitChoice = submenus.size() + 1;

    // Set up chaining for the printer mock
    when(printer.inline()).thenReturn(printer);
    when(printer.formatAsList(any())).thenReturn(printer);

    mainMenu = new MainMenu(printer, prompt, submenus);
  }

  @Test
  public void shouldBeAbleToLoadASubmenuByChoice() {
    Integer choice = 2;
    when(prompt.getChoice(anyInt()))
      .thenReturn(choice)
      .thenReturn(exitChoice);

    mainMenu.load();

    verify(submenus.get(choice - 1), times(1)).load();
  }

  @Test
  public void shouldBeAbleToExit() {
    when(prompt.getChoice(anyInt())).thenReturn(exitChoice);

    mainMenu.load();

    // Verify load is never called for any of the submenus
    // IE verify the exit option does not load any submenu
    submenus.forEach(sm -> verify(sm, never()).load());
  }

  @Test
  public void shouldShowWelcomeMessageOnFirstLoad() {
    // Just have choice be exit
    when(prompt.getChoice(anyInt())).thenReturn(exitChoice);

    mainMenu.load();

    verify(printer, times(1)).print(GameConfiguration.welcomeMessaage);
  }

  @Test
  public void shouldNotShowWelcomeAfterFirstLoad() {
    // run through all the submenus
    // for good measure
    when(prompt.getChoice(anyInt()))
      .thenReturn(1)
      .thenReturn(2)
      .thenReturn(3)
      .thenReturn(exitChoice);

    mainMenu.load();

    verify(printer, times(1)).print(GameConfiguration.welcomeMessaage);
  }
}