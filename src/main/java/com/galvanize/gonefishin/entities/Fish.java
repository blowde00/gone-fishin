package com.galvanize.gonefishin.entities;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
public class Fish {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Bait> bait;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Location> location;

    private String species;
    private String fishSize;
    private Integer reward;
    private Integer fishStrength;
    @Column(columnDefinition = "text")
    private String art;


    public Fish() {}

    public Fish(String species, String fishSize, Integer reward, Integer fishStrength, List<Bait> bait, List<Location> location, String art) {
        this.species = species;
        this.fishSize = fishSize;
        this.reward = reward;
        this.fishStrength = fishStrength;
        this.bait = bait;
        this.location = location;
        this.art = art;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getFishSize() {
        return fishSize;
    }

    public void setFishSize(String fishSize) {
        this.fishSize = fishSize;
    }

    public Integer getReward() {
        return reward;
    }

    public void setReward(Integer reward) {
        this.reward = reward;
    }

    public Integer getFishStrength() {
        return fishStrength;
    }

    public void setFishStrength(Integer fishStrength) {
        this.fishStrength = fishStrength;
    }

    public void setBait(List<Bait> bait) {
        this.bait = bait;
    }

    public List<Bait> getBait() {
        return bait;
    }

    public List<Location> getLocation() {
        return location;
    }

    public void setLocation(List<Location> location) {
        this.location = location;
    }

    public String getArt() {return art;}

    public void setArt(String art) {this.art = art;}
}
