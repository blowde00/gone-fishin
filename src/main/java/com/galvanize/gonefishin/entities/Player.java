package com.galvanize.gonefishin.entities;

import javax.persistence.*;

@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    private Bait bait;

    @OneToOne(fetch = FetchType.EAGER)
    private Location location;

    @OneToOne(fetch = FetchType.EAGER)
    private FishingRod fishingRod;

    private Integer bank;

    private Boolean isHookBaited = true;

    public Player() {}

    public Player(FishingRod fishingRod, Bait bait, Location location, Integer bank) {
        this.bait = bait;
        this.location = location;
        this.fishingRod = fishingRod;
        this.bank = bank;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public Integer getBank() {
        return bank;
    }

    public Bait getBait() {
        return bait;
    }

    public Location getLocation() {
        return location;
    }

    public FishingRod getFishingRod() {
        return fishingRod;
    }

    public void setBait(Bait bait) {
        this.bait = bait;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setFishingRod(FishingRod fishingRod) {
        this.fishingRod = fishingRod;
    }

    public void setBank(Integer bank) {
        this.bank = bank;
    }

    public Boolean getHookBaited() {
        return isHookBaited;
    }

    public void setHookBaited(Boolean hookBaited) {
        isHookBaited = hookBaited;
    }
}
