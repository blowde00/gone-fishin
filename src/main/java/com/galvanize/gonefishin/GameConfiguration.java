package com.galvanize.gonefishin;

import com.galvanize.gonefishin.data.entities.Setting;
import com.galvanize.gonefishin.menu.*;
import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import com.galvanize.gonefishin.data.SettingType;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
 * This is the main configuration file for logic-level
 * configuration, such as static values used throughout
 * the application of bean factories we create for Spring
 * to use.
 */
@Configuration
public class GameConfiguration {

    public static String welcomeMessaage =
            "+------------------------+%n" +
                    "| <purple>Welcome</purple> to <bold orange>GONE FISHIN</bold orange> |%n" +
                    "+------------------------+";

    // Provides a scanner we can autowire in other classes
    @Bean
    public Scanner getScanner() {
        return new Scanner(System.in);
    }

    // Default list of settings available in the game
    @Bean
    public List<Setting> getGameSettings() {
        return List.of(
                new Setting("String Setting", SettingType.STRING, "A Value!"),
                new Setting("Number Setting", SettingType.NUMBER, "10"),
                new Setting("Binary Setting", SettingType.BINARY, "TRUE")
        );
    }

    /*
     * This bean is used as the main menu of the game. It will be
     * loaded as the start of the player experience.
     */
    @Bean("mainmenu")
    public Menu getRootMenus(ApplicationContext context, Printer printer, Prompt prompt) {
        List<Menu> rootSubmenus = new ArrayList<>(List.of(
            context.getBean(FishingMenu.class),
            context.getBean(TackleBoxMenu.class),
            context.getBean(TravelMenu.class),
            context.getBean(ShoppingMenu.class)
        ));

        return new MainMenu(printer, prompt, rootSubmenus);
    }


}
