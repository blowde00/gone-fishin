package com.galvanize.gonefishin.data;

import com.galvanize.gonefishin.data.entities.Setting;
import com.galvanize.gonefishin.data.repositories.SettingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class SettingsManagerTest {

  @Autowired
  private SettingRepository repository;

  /*
   * Creates a new setting object that can be used in testing
   */
  private Setting createTestSetting(String name) {
    return new Setting(name, SettingType.STRING, "Default");
  }

  private Setting createTestSetting(String name, SettingType type, String defaultValue) {
    return new Setting(name, type, defaultValue);
  }

  /*
   * Helper that creates a Settings Manager instance
   * that is initialized with the given settings
   */
  private SettingsManager createManager(Setting ...settings) {
    return new SettingsManager(List.of(settings), repository);
  }

  /*
   * Special manager that is used for testing value conversion methods
   */
  private SettingsManager createConversationManager(SettingType typeToTest) {
    return createManager(
      createTestSetting("String Type"),
      createTestSetting("String Value", typeToTest, "this is a string"),
      createTestSetting("Empty Value", typeToTest, ""),
      createTestSetting("Null Value", typeToTest, null)
    );
  }

  @BeforeEach
  public void testSetup() {
    repository.deleteAll();
  }

  @Test
  public void getSettingShouldReturnNullForNonexistentSettings() {
    SettingsManager manager = createManager();
    assertNull(manager.getSetting("Setting Does Not Exist"));
  }

  @Test
  public void shouldBeAbleToLoadSavedSettings() {
    String settingName = "Test Setting";
    Setting originalSetting = createTestSetting(settingName);
    repository.save(originalSetting);

    // Provide an unsaved setting (without an id) that
    // will be replaced with one from the database
    // when load is called, based on the name
    Setting unsavedSetting = createTestSetting(settingName);
    SettingsManager manager = createManager(unsavedSetting);
    assertNull(manager.getSetting(settingName).getSettingId());

    manager.load();
    assertEquals(
      originalSetting.getSettingId(),
      manager.getSetting(settingName).getSettingId()
    );
  }

  @Test
  public void shouldSaveNewSettingsOnLoadWithDefaultValues() {
    String settingName = "Test Setting";
    SettingsManager manager = createManager(createTestSetting(settingName));
    assertNull(manager.getSetting(settingName).getSettingId());

    manager.load();
    Setting managedSetting = manager.getSetting(settingName);
    assertNotNull(managedSetting.getSettingId());

    Optional<Setting> savedSetting = repository.findById(managedSetting.getSettingId());
    assertTrue(savedSetting.isPresent());
    assertEquals(
      managedSetting.getDefaultValue(),
      savedSetting.get().getValue()
    );
  }

  @Test
  public void shouldBeAbleToGetABooleanValueCaseInsensitive() {
    String falseSettingName = "False Setting";
    String trueSettingName = "True Setting";

    SettingsManager manager = createManager(
      createTestSetting(falseSettingName, SettingType.BINARY, "fAlsE"),
      createTestSetting(trueSettingName, SettingType.BINARY, "truE")
    );

    assertTrue(manager.getBinary(trueSettingName));
    assertFalse(manager.getBinary(falseSettingName));
  }

  @Test
  public void shouldBeAbleToGetNumberValues() {
    String intSettingName = "99c Long Distance";
    String negativeIntSettingName = "50% Off!";
    String floatSettingName = "Pi";
    String negativeFloatSettingName = "Negative Pi";

    SettingsManager manager = createManager(
      createTestSetting(intSettingName, SettingType.NUMBER, "1010220"),
      createTestSetting(negativeIntSettingName, SettingType.NUMBER, "-50"),
      createTestSetting(floatSettingName, SettingType.NUMBER, "3.14"),
      createTestSetting(negativeFloatSettingName, SettingType.NUMBER, "-3.14")
    );

    assertEquals(1010220, manager.getInteger(intSettingName));
    assertEquals(-50, manager.getInteger(negativeIntSettingName));
    assertEquals(3.14f, manager.getFloat(floatSettingName));
    assertEquals(-3.14f, manager.getFloat(negativeFloatSettingName));
  }

  @Test
  public void getBinaryShouldReturnNullOnError() {
    SettingsManager manager = createConversationManager(SettingType.BINARY);

    assertNull(manager.getBinary("Does Not Exist"));
    manager
      .getSettingNames()
      .forEach(n -> assertNull(manager.getBinary(n)));
  }

  @Test
  public void getNumberMethodsShouldReturnNullOnError() {
    SettingsManager manager = createConversationManager(SettingType.NUMBER);

    assertNull(manager.getInteger("Does Not Exist"));
    assertNull(manager.getFloat("Does Not Exist"));
    manager
      .getSettingNames()
      .forEach(n -> {
        assertNull(manager.getInteger(n));
        assertNull(manager.getFloat(n));
      });
  }

  @Test
  public void shouldBeAbleToGetStringValue() {
    String settingName = "Test Setting";
    String settingValue = "Default String Value";

    SettingsManager manager = createManager(
      createTestSetting(settingName, SettingType.STRING, settingValue)
    );

    assertEquals(settingValue, manager.getValue(settingName));
  }

  @Test
  public void getValueShouldReturnNullForNonexistentSettings() {
    SettingsManager manager = createManager();
    assertNull(manager.getValue("Does Not Exist"));
  }

  @Test
  public void shouldBeAbleToSetValues() {
    String stringSettingName = "String Setting";
    String binarySettingName = "Binary Setting";
    String intSettingName = "Integer Setting";
    String floatSettingName = "Float Setting";
    String stringValue = "This is a string!";
    Boolean binaryValue = false;
    Integer intValue = 1010;
    Float floatValue = 3.14f;

    SettingsManager manager = createManager(
      createTestSetting(stringSettingName),
      createTestSetting(binarySettingName, SettingType.BINARY, "true"),
      createTestSetting(intSettingName, SettingType.NUMBER, "10"),
      createTestSetting(floatSettingName, SettingType.NUMBER, "22.22")
    );

    assertNotEquals(stringValue, manager.getValue(stringSettingName));
    assertNotEquals(binaryValue, manager.getBinary(binarySettingName));
    assertNotEquals(intValue, manager.getInteger(intSettingName));
    assertNotEquals(floatValue, manager.getFloat(floatSettingName));

    assertTrue(manager.set(stringSettingName, stringValue));
    assertTrue(manager.set(binarySettingName, binaryValue));
    assertTrue(manager.set(intSettingName, intValue));
    assertTrue(manager.set(floatSettingName, floatValue));

    assertEquals(stringValue, manager.getValue(stringSettingName));
    assertEquals(binaryValue, manager.getBinary(binarySettingName));
    assertEquals(intValue, manager.getInteger(intSettingName));
    assertEquals(floatValue, manager.getFloat(floatSettingName));
  }

  @Test
  public void setShouldRevertAndReturnFalseOnIncompatibleTypes() {
    String binarySettingName = "Binary Setting";
    String intSettingName = "Integer Setting";
    String floatSettingName = "Float Setting";

    SettingsManager manager = createManager(
      createTestSetting(binarySettingName, SettingType.BINARY, "true"),
      createTestSetting(intSettingName, SettingType.NUMBER, "10"),
      createTestSetting(floatSettingName, SettingType.NUMBER, "22.22")
    );

    Boolean binaryValue = manager.getBinary(binarySettingName);
    Integer intValue = manager.getInteger(intSettingName);
    Float floatValue = manager.getFloat(floatSettingName);

    assertFalse(manager.set(binarySettingName, "something"));
    assertFalse(manager.set(intSettingName, "something"));
    assertFalse(manager.set(floatSettingName, "something"));

    assertEquals(binaryValue, manager.getBinary(binarySettingName));
    assertEquals(intValue, manager.getInteger(intSettingName));
    assertEquals(floatValue, manager.getFloat(floatSettingName));
  }

  @Test
  public void setShouldReturnFalseOnNonexistentSetting() {
    SettingsManager manager = createManager();
    assertFalse(manager.set("Does Not Exist", ""));
  }

  @Test
  public void shouldBeAbleToSaveASettingToTheDatabase() {
    String settingName = "Test Setting";
    String newValue = "Changed Value!";
    Setting setting = createTestSetting("Test Setting");
    Setting unsavedSetting = createTestSetting("Unsaved Test Setting!");
    SettingsManager manager = createManager(
      setting,
      unsavedSetting
    );

    manager.load();

    Optional<Setting> postLoadSetting = repository.findById(setting.getSettingId());
    assertTrue(postLoadSetting.isPresent());
    assertEquals(setting.getDefaultValue(), postLoadSetting.get().getValue());

    manager.set(settingName, newValue);
    manager.set(unsavedSetting.getName(), newValue);
    manager.save(settingName);

    Optional<Setting> postSaveSetting =
      repository.findById(setting.getSettingId());

    Optional<Setting> postSaveUnsavedSetting =
      repository.findById(unsavedSetting.getSettingId());

    assertTrue(postSaveSetting.isPresent());
    assertTrue(postSaveUnsavedSetting.isPresent());

    assertEquals(newValue, postSaveSetting.get().getValue());
    assertNotEquals(
      unsavedSetting.getValue(),
      postSaveUnsavedSetting.get().getValue()
    );
  }

  @Test
  public void shouldBeAbleToSaveAllSettingsToTheDatabase() {
    String defaultValue = "Default!";
    String changedValue = "Changed!";
    SettingsManager manager = createManager(
      createTestSetting("Setting One", SettingType.STRING, defaultValue),
      createTestSetting("Setting 2", SettingType.STRING, defaultValue),
      createTestSetting("Setting III", SettingType.STRING, defaultValue)
    );

    manager.getSettingNames().forEach(n -> manager.set(n, changedValue));
    manager.save();

    List<Setting> savedSettings = repository.findAll();
    assertEquals(3, savedSettings.size());
    savedSettings.forEach(s -> {
      assertEquals(manager.getValue(s.getName()), s.getValue());
      assertEquals(changedValue, s.getValue());
    });
  }
}