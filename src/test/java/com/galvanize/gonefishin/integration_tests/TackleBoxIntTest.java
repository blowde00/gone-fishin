package com.galvanize.gonefishin.integration_tests;

import com.galvanize.gonefishin.entities.*;
import com.galvanize.gonefishin.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TackleBoxIntTest {

    @Autowired
    BaitRepository baitRepository;

    @Autowired
    TackleBoxRepository tackleBoxRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    RodRepository rodRepository;

    @Test
    public void canAddToTackleBoxRepository() {
        Bait bait = new Bait("worm", true, 1);
        Bait baitResult = baitRepository.save(bait);

        Location location = new Location("Matt's Wacky Pond", "Pond", true);
        Location locationResult = locationRepository.save(location);

        FishingRod fishingRod = new FishingRod("Uncle David's Hand Me Down", 1, 0);
        FishingRod fishingRodResult = rodRepository.save(fishingRod);

        Player player = new Player(fishingRodResult, baitResult, locationResult, 10);
        Player playerResult = playerRepository.save(player);

        TackleBox tackleBox = new TackleBox(bait, 3, playerResult);
        assertNull(tackleBox.getId());
        TackleBox tackleResult = tackleBoxRepository.save(tackleBox);
        assertNotNull(tackleBox.getId());

        assertEquals(tackleBox.getBait(), tackleResult.getBait());

        Optional<TackleBox> possibleTackleBox = tackleBoxRepository.findById(tackleResult.getId());
        assertTrue(possibleTackleBox.isPresent());
        TackleBox foundTackleBox = possibleTackleBox.get();
        assertEquals(tackleBox.getBait().getType(), foundTackleBox.getBait().getType());
        assertEquals(tackleBox.getQuantity(), foundTackleBox.getQuantity());
    }
}
