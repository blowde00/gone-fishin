package com.galvanize.gonefishin.repositories;

import com.galvanize.gonefishin.entities.TackleBox;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TackleBoxRepository extends CrudRepository<TackleBox, Integer> {
}
