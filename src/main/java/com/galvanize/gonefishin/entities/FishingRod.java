package com.galvanize.gonefishin.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class FishingRod {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private Integer rodStrength;
    private Integer cost;

    public FishingRod() {}

    public FishingRod(String name, Integer rodStrength, Integer cost) {
        this.name = name;
        this.rodStrength = rodStrength;
        this.cost = cost;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRodStrength() {
        return rodStrength;
    }

    public void setRodStrength(Integer rodStrength) {
        this.rodStrength = rodStrength;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
