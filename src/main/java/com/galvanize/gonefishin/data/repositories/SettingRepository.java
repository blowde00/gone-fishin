package com.galvanize.gonefishin.data.repositories;

import com.galvanize.gonefishin.data.entities.Setting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface SettingRepository extends CrudRepository<Setting, UUID> {

  @Override
  List<Setting> findAll();



}
