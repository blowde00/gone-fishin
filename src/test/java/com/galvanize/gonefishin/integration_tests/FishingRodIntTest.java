package com.galvanize.gonefishin.integration_tests;

import com.galvanize.gonefishin.entities.FishingRod;
import com.galvanize.gonefishin.repositories.RodRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class FishingRodIntTest {

    @Autowired
    RodRepository rodRepository;

    @Test
    public void addRodToRepo() {
        FishingRod rod = new FishingRod("Uncle Davids Hand Me Down", 1, 0);
        FishingRod result = rodRepository.save(rod);
        assertEquals(rod.getName(), result.getName());

        Optional<FishingRod> possibleRod = rodRepository.findById(result.getId());
        assertTrue(possibleRod.isPresent());
        FishingRod foundRod = possibleRod.get();
        assertEquals(rod.getName(), foundRod.getName());
    }

}
