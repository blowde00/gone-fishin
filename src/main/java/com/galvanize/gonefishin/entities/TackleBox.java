package com.galvanize.gonefishin.entities;

import javax.persistence.*;

@Entity
public class TackleBox {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    private Bait bait;

    @ManyToOne(fetch = FetchType.EAGER)
    private Player player;

    private Integer quantity = 0;

    public TackleBox() {}

    public TackleBox(Bait bait, Integer quantity, Player player) {
        this.bait = bait;
        this.quantity = quantity;
        this.player = player;

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public Bait getBait() {
        return bait;
    }

    public void setBait(Bait bait) {
        this.bait = bait;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
//        TODO: add condition to determine increment or decrement
        this.quantity = quantity;
    }

    public void addBaitQuantity(Integer quantity) {
        this.quantity += quantity;
    }
}
