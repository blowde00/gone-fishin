package com.galvanize.gonefishin.integration_tests;

import com.galvanize.gonefishin.entities.Location;
import com.galvanize.gonefishin.repositories.LocationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class LocationIntTest {

    @Autowired
    LocationRepository locationRepository;

    @Test
    public void canSaveLocation() {
        Location location = new Location("Specific Ocean", "Ocean", false);
        assertNull(location.getId());
        Location result = locationRepository.save(location);
        assertNotNull(location.getId());

        assertEquals(location.getType(), result.getType());

        Optional<Location> actual = locationRepository.findById(result.getId());
        assertTrue(actual.isPresent());
        assertEquals(location.getType(), actual.get().getType());
    }
}
