package com.galvanize.gonefishin.data.entities;

import com.galvanize.gonefishin.data.SettingType;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Setting {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID settingId;

  @Enumerated(EnumType.ORDINAL)
  private SettingType type;

  @Column(unique = true)
  private String name;

  private String value;

  // We don't want to save this field to the database
  // because the database only needs a value
  @Transient
  private String defaultValue;

  public Setting() {}

  public Setting(String name, SettingType type, String defaultValue) {
    this.name = name;
    this.type = type;
    this.defaultValue = defaultValue;
    this.value = this.defaultValue;
  }

  public UUID getSettingId() {
    return settingId;
  }

  public void setSettingId(UUID settingId) {
    this.settingId = settingId;
  }

  public Boolean hasId() {
    return getSettingId() != null;
  }

  public SettingType getType() {
    return type;
  }

  public Boolean isType(SettingType targetType) {
    return type == targetType;
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return (value != null ? value : defaultValue);
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public void copyFrom(Setting settingToCopy) {
    setSettingId(settingToCopy.getSettingId());
    setValue(settingToCopy.getValue());
  }
}
