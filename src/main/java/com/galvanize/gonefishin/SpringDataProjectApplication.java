package com.galvanize.gonefishin;

import com.galvanize.gonefishin.data.Seeder;
import com.galvanize.gonefishin.menu.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * To properly implement a terminal-based application with Spring
 * we implement CommandLineRunner. This can be done in an external
 * class but for convenience we use the main class instead.
 */
@SpringBootApplication
public class SpringDataProjectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataProjectApplication.class, args);
	}

	// This comes from the application-test.yml file
	// or is false by default
	@Value("${skip-initial-load:false}")
	private Boolean skipLoad;

	private final Menu mainMenu;
	private final Seeder seeder;

	@Autowired
	public SpringDataProjectApplication(@Qualifier("mainmenu") Menu mainMenu, Seeder seeder) {
		this.mainMenu = mainMenu;
		this.seeder = seeder;
	}

	/*
	 * This is the entry point to the terminal application.
	 */
	@Override
	public void run(String... args) {
		if(skipLoad) return;

		seeder.seed();

		mainMenu.load();
	}
}
