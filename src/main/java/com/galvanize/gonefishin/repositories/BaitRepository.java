package com.galvanize.gonefishin.repositories;

import com.galvanize.gonefishin.entities.Bait;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BaitRepository extends CrudRepository<Bait, Integer> {
    Optional<Bait> findByType(String type);
}
