package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.entities.Bait;
import com.galvanize.gonefishin.entities.FishingRod;
import com.galvanize.gonefishin.entities.Player;
import com.galvanize.gonefishin.entities.TackleBox;
import com.galvanize.gonefishin.repositories.BaitRepository;
import com.galvanize.gonefishin.repositories.PlayerRepository;
import com.galvanize.gonefishin.repositories.RodRepository;
import com.galvanize.gonefishin.repositories.TackleBoxRepository;
import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class ShoppingMenu extends Menu {

    private final String separator = "-".repeat(12);

    @Autowired
    private BaitRepository baitRepository;

    @Autowired
    private RodRepository rodRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private TackleBoxRepository tackleBoxRepository;

    public ShoppingMenu(Printer printer, Prompt prompt) {
        super(printer, prompt, "Fishin Store",
            new String[] {
                "Buy Bait",
                "Buy Rod"
        });
//        this.baitRepository = baitRepository;
    }

    @Override
    public void load() {
        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player = players.get(0);

        Iterable<Bait> baitIterable = baitRepository.findAll();
        List<Bait> baitList = Streamable.of(baitIterable).toList();
        ArrayList<Bait> baitArrayList = new ArrayList<>(baitList);
        Bait[] baitArray = baitArrayList.toArray(Bait[]::new);

        Iterable<FishingRod> rodIterable = rodRepository.findAll();
        List<FishingRod> rodList = Streamable.of(rodIterable).toList();
        ArrayList<FishingRod> rodArrayList = new ArrayList<>(rodList);
        FishingRod[] rodArray = rodArrayList.toArray(FishingRod[]::new);

        printer.print("%n<bold cyan>Fishin Store</bold cyan>");
        printer.print(separator);
        printer.print(
                "<green underline>Bank Balance: $%s</green underline>%n",
                player.getBank()
        );

        Integer choice = promptOptions();
        if(isExit(choice)) return;

            switch(choice) {
                case 1:
                    printer.print("%n<bold yellow underline>Bait Selection</bold yellow underline>");
                    for(int i = 0; i < baitArrayList.size(); i++) {
                        Bait b = baitArrayList.get(i);

                        printer.print(
                              "  [%d] %s (<green>$%s</green>)",
                              i + 1,
                              b.getType(),
                              b.getCost()
                        );
                    }
                    printer.print("  [11] Exit");
                    printer.inline().print("Which Bait");
                    int action = prompt.getChoice(baitArrayList.size() + 1);
                    if (action == baitArrayList.size() + 1) {
                        load();
                    } else {
                        buyBait(
                                baitArray[action - 1]
                        );
                    }
                    break;
                case 2:
                    printer.print("%n<bold yellow underline>Rod Selection</bold yellow underline>");
                    for(int j = 0; j < rodArrayList.size(); j++) {
                        FishingRod r = rodArrayList.get(j);

                        printer.print(
                                "  [%d] %s (<green>$%s</green>)",
                                j + 1,
                                r.getName(),
                                r.getCost()
                        );

                    }
                    printer.print("  [6] Exit");
                    printer.inline().print("Which Rod");
                    int action1 = prompt.getChoice(rodArrayList.size() + 1);
                    if (action1 == rodArrayList.size() + 1) {
                        return;
                    } else {
                        buyRod(
                                rodArray[action1 - 1]
                        );
                    }
                    break;
                default:
                    printer.print("What the heck are you doing");
            }


    }

    private void buyBait(Bait bait) {
        printer.inline().print("How Many");
        Integer buyQuantity;
        buyQuantity = prompt.getChoice(100);

        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player1 = players.get(0);

        Iterable<TackleBox> tackleBoxIterable = tackleBoxRepository.findAll();
        List<TackleBox> tackleBoxInventory = Streamable.of(tackleBoxIterable).toList();
        ArrayList<TackleBox> tackleBoxArrayList = new ArrayList<>(tackleBoxInventory);

        if (player1.getBank() >= (bait.getCost() * buyQuantity)) {
            Integer newBank = player1.getBank() - (bait.getCost() * buyQuantity);
            player1.setBank(newBank);
            playerRepository.save(player1);

            boolean matchesBait = false;
            for (int i = 0; i < tackleBoxArrayList.size(); i++) {
                if (tackleBoxArrayList.get(i).getBait().getType().equals(bait.getType())) {
                    matchesBait = true;
                    TackleBox item = tackleBoxArrayList.get(i);
                    item.addBaitQuantity(buyQuantity);
                    tackleBoxRepository.saveAll(tackleBoxArrayList);
                    printer.print(
                            "Thanks for your purchase. Come back any time!"
                    );
                }
            }
            if (matchesBait == false) {
                tackleBoxArrayList.add(new TackleBox(baitRepository.findByType(bait.getType()).get(), buyQuantity, player1));
                tackleBoxRepository.saveAll(tackleBoxArrayList);
                printer.print(
                        "Thanks for your purchase. Come back any time!"
                );
            }

        } else {
            Integer bankNeeded = (bait.getCost() * buyQuantity) - player1.getBank();
            printer.print("Sorry partner! Looks like you're <green>$%d</green> short. Come back when you catch some more fish.",
                    bankNeeded
            );
        }
        load();
    }

    private void buyRod(FishingRod rod) {
        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player1 = players.get(0);

        if (player1.getFishingRod().getName().equals(rod.getName())) {
            printer.print("Sorry partner! Looks like that's the rod you're already using.");
        } else {
            if (player1.getBank() >= rod.getCost()) {
                Integer newBank = player1.getBank() - rod.getCost();
                player1.setBank(newBank);
                player1.setFishingRod(rod);
                playerRepository.save(player1);
                if (rod.getCost() == 0) {
                    printer.print("Well...I guess I can dust off this ol' thing and hand it over to you. Good luck!");
                } else {
                    printer.print(
                            "Thanks for purchasing %s for <green>$%s</green>. Come back any time!",
                            player1.getFishingRod().getName(),
                            player1.getFishingRod().getCost()
                    );
                }
            } else {
                Integer bankNeeded = rod.getCost() - player1.getBank();
                printer.print("Sorry partner! Looks like you're <green>$%d</green> short. Come back when you catch some more fish.",
                        bankNeeded
                );
            }
        }
        load();
    }
}
