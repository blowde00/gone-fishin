package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.entities.Player;
import com.galvanize.gonefishin.entities.TackleBox;
import com.galvanize.gonefishin.repositories.PlayerRepository;
import com.galvanize.gonefishin.repositories.TackleBoxRepository;
import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Component;
import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class TackleBoxMenu extends Menu{

    private final String separator = "-".repeat(10);

    @Autowired
    private TackleBoxRepository tackleBoxRepository;

    public TackleBoxMenu(Printer printer, Prompt prompt) {
        super(printer, prompt, "Tackle Box", new String[] {
        });
    }

    @Override
    public void load() {

        Iterable<TackleBox> tackleBoxIterable = tackleBoxRepository.findAll();
        List<TackleBox> tackleBoxInventory = Streamable.of(tackleBoxIterable).toList();
        ArrayList<TackleBox> tackleBoxArrayList = new ArrayList<>(tackleBoxInventory);

        printer.print("%n<bold cyan>Tackle Box</bold cyan>");
        printer.print(separator);
        if (tackleBoxArrayList.size() == 0) {
            printer.print("%nYour tackle box is <red>empty</red>!");
        } else {
            for (int i = 0; i < tackleBoxArrayList.size(); i++) {
                TackleBox t = tackleBoxArrayList.get(i);

                printer.print(
                        "  %s (%s)",
                        t.getBait().getType(),
                        t.getQuantity());
            }
        }
    }
}

