package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.data.SettingsManager;
import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class SettingsMenuTest {

  @MockBean
  Printer printer;

  @MockBean
  Prompt prompt;

  @MockBean
  SettingsManager manager;

  SettingsMenu menu;

  @BeforeEach
  public void setupTest() {
    // Set up chaining for the printer mock
    when(printer.inline()).thenReturn(printer);
    when(printer.formatAsList(any())).thenReturn(printer);
    when(printer.multiline(any())).thenReturn(printer);

    menu = new SettingsMenu(printer, prompt, manager);
  }

  @Test
  public void shouldBeAbleToExit() {
    when(manager.getSettingNames()).thenReturn(new ArrayList<>());
    when(prompt.getChoice(anyInt())).thenReturn(menu.getExitChoice());

    menu.load();

    // Verify that it exits before prompting to pick a setting
    verify(printer, never()).print("Which Setting");
  }

  @Test
  public void shouldBeAbleToSetASetting() {
    String settingName = "Test Setting";
    String settingValue = "Test Value!";

    when(manager.getSettingNames()).thenReturn(List.of(
      settingName
    ));
    when(prompt.getChoice(anyInt()))
      .thenReturn(1)
      .thenReturn(1)
      .thenReturn(menu.getExitChoice());
    when(prompt.getString()).thenReturn(settingValue);
    when(manager.set(settingName, settingValue)).thenReturn(true);

    menu.load();

    verify(manager, times(1))
      .set(settingName, settingValue);

    verify(manager, times(1))
      .save(settingName);
  }

  @Test
  public void shouldKeepPromptingUntilValidSettingValue() {
    String settingName = "Test Setting";
    String[] settingValues = new String[] {
      "Test Value One!",
      "Test Value II!!",
      "Test Value 3!!!"
    };

    when(manager.getSettingNames()).thenReturn(List.of(
      settingName
    ));
    when(prompt.getChoice(anyInt()))
      .thenReturn(1)
      .thenReturn(1)
      .thenReturn(menu.getExitChoice());
    when(prompt.getString())
      .thenReturn(settingValues[0])
      .thenReturn(settingValues[1])
      .thenReturn(settingValues[2]);
    when(manager.set(eq(settingName), anyString()))
      .thenReturn(false)
      .thenReturn(false)
      .thenReturn(true);

    menu.load();

    verify(manager, times(3))
      .set(eq(settingName), anyString());

    verify(printer, times(2))
      .print(Prompt.invalidInputMessage);
  }

  /*
   * This test helps get the coverage up.
   */
  @Test
  public void shouldBeAbleToGetOptionByPromptChoice() {
    Integer targetChoice = 1;
    when(prompt.getChoice(anyInt()))
      .thenReturn(targetChoice);

    Integer choice = menu.promptOptions();

    assertEquals(
      menu.getOption(0),
      menu.getOptionByChoice(targetChoice)
    );
  }
}