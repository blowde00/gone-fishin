package com.galvanize.gonefishin.menu;

import com.galvanize.gonefishin.entities.*;
import com.galvanize.gonefishin.repositories.BaitRepository;
import com.galvanize.gonefishin.repositories.FishRepository;
import com.galvanize.gonefishin.repositories.PlayerRepository;
import com.galvanize.gonefishin.repositories.TackleBoxRepository;
import com.galvanize.gonefishin.utils.terminal.Printer;
import com.galvanize.gonefishin.utils.terminal.Prompt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Component
public class FishingMenu extends Menu {

    private final String separator = "-";

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TackleBoxRepository tackleBoxRepository;

    @Autowired
    ShoppingMenu shoppingMenu;

    @Autowired
    FishRepository fishRepository;

    @Autowired
    BaitRepository baitRepository;

    private String baitStolen = " ^\n" +
            "       //                        ___   ___\n" +
            "     (*)     \"O\"                /  _   _  \\\n" +
            "    (*)                           / \\ / \\\n" +
            "   (*)    \"O\"                    |   |   |    |\\\n" +
            "  //                             |O  |O  |___/  \\     ++\n" +
            " //                               \\_/ \\_/    \\   | ++\n" +
            "//                              _/      __    \\  \\\n" +
            "/     /|   /\\                  (________/ __   |_/\n" +
            "     / |  |  |                   (___      /   |    |\\\n" +
            "    / /  /   |                     \\     \\|    |___/  |\n" +
            "   |  | |   /                       \\_________      _/   ++++\n" +
            "  /   | |  |                      ++           \\    |\n" +
            " |   / /   |                              ++   |   /  +++\n" +
            "/   /  |   |                               ++ /__/\n" +
            "~~~ ~~~~   ~~~~~~~~~~~~  ~~~~~~~~~~~~~  ~~~~        ~~+++~~~~ ~";

    private String sadPerson =
            "      ////^\\\\\\\\\n" +
            "      | ^   ^ |\n" +
            "     @ (o) (o) @\n" +
            "      |   <   |\n" +
            "      |  ___  |\n" +
            "       \\_____/\n" +
            "     ____|  |____\n" +
            "    /    \\__/    \\\n" +
            "   /              \\\n" +
            "  /\\_/|        |\\_/\\\n" +
            " / /  |        |  \\ \\\n" +
            "( <   |        |   > )\n" +
            " \\ \\  |        |  / /\n" +
            "  \\ \\ |________| / /\n" +
            "   \\ \\|";

    private String troll =
                    "░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░\n" +
                    "░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░\n" +
                    "░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░\n" +
                    "░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░\n" +
                    "░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░\n" +
                    "█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█\n" +
                    "█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█\n" +
                    "░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░\n" +
                    "░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░\n" +
                    "░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░\n" +
                    "░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░\n" +
                    "░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░\n" +
                    "░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░\n" +
                    "░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░\n" +
                    "░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░";

    public FishingMenu(Printer printer, Prompt prompt) {
        super(printer, prompt, "Let's Fish!", new String[]{
                "Bait Hook",
                "Cast"
        });
    }

    @Override
    public void load() {
        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player = players.get(0);
        Integer rodStrength = player.getFishingRod().getRodStrength();
        String currentLocation = player.getLocation().getName();
        String fishingRod = player.getFishingRod().getName();
        String currentBait = player.getBait().getType();

        printer.print("%n<blue>%s</blue>",currentLocation);
        printer.print(separator.repeat(currentLocation.length()));
        if (player.getHookBaited()) {
            printer.multiline("Rod: %s", "Bait: %s%n").print(fishingRod, currentBait);
        } else {
            printer.multiline("Rod: %s", "Bait: <bold red>Empty </bold red>%n").print(fishingRod);
        }

        Integer choice = promptOptions();
        if (isExit(choice)) return;

        switch (choice) {
            case 1:
                Iterable<TackleBox> tackleBoxIterable = tackleBoxRepository.findAll();
                List<TackleBox> tackle = Streamable.of(tackleBoxIterable).toList();
                ArrayList<TackleBox> tackleBoxArrayList = new ArrayList<>(tackle);
                if (tackleBoxArrayList.size() == 0 && !player.getBank().equals(0)) {
                    if (player.getHookBaited()) {
                        printer.print("%nYou have no more bait in your tackle box. You can keep fishing with what's on your hook or go shopping for more.");
                        load();
                    } else {
                        printer.print("%nYou have no more bait in your tackle box. You'll need to buy some more.");
                        shoppingMenu.load();
                    }
                } else if (tackleBoxArrayList.size() == 0 && player.getBank().equals(0) && !player.getHookBaited()) {
                    printer.print("%nYou're out of money and out of bait. <red>Game Over!</red>");
                    printer.wait(3);
                    System.exit(0);
                } else {
                    printer.print("%n<underline yellow>Tackle Box</underline yellow>");
                    for (int i = 0; i < tackleBoxArrayList.size(); i++) {
                        TackleBox t = tackleBoxArrayList.get(i);

                        printer.print(
                                "  [%d] %s (%s)",
                                i + 1,
                                t.getBait().getType(),
                                t.getQuantity());
                    }
                    printer.inline().print("Which Bait");
                    int action = prompt.getChoice(tackleBoxArrayList.size());
                    baitHook(tackleBoxArrayList.get(action - 1));
                }
                break;
            case 2:
                //set up the fish in location that like the bait we have loaded
                if (player.getHookBaited()) {

                    Iterable<Fish> fishIterable = fishRepository.findAll();
                    List<Fish> fishList = Streamable.of(fishIterable).toList();
                    ArrayList<Fish> fishArrayList = new ArrayList<>(fishList);

                    List<Fish> fishInLocationsThatLikesBait = new ArrayList<>();

                    for (int i = 0; i < fishArrayList.size(); i++) {
                        List<Location> fishLocations = fishArrayList.get(i).getLocation();
                        for (int j = 0; j < fishLocations.size(); j++) {
                            if (fishLocations.get(j).getName().equals(currentLocation)) {
                                List<Bait> baitFishLikes = fishArrayList.get(i).getBait();
                                for (int k = 0; k < baitFishLikes.size(); k++) {
                                    if (baitFishLikes.get(k).getType().equals(player.getBait().getType())) {
                                        fishInLocationsThatLikesBait.add(fishArrayList.get(i));
                                    }
                                }
                            }
                        }
                    }

                    printer.print("%n<bold orange>Good Luck!</bold orange>%n");
                    printer.wait(1);
                    printer.print("Ziiiiinnnnggg...splash! <orange>Great cast!</orange>");
                    printer.wait(1);
                    printer.print("Waiting for a nibble...any second now");
                    printer.wait(3);
                    Random random = new Random();
                    int castResult = random.nextInt(6);

                    switch (castResult) {
                        case 1:
                        case 2:
                        case 3:
                            catchFish(fishInLocationsThatLikesBait, rodStrength);
                            break;
                        case 4:  //hook a fish but it gets away with the bait
                            printer.print("Woo hoo, I think it's a bite! Set the hook and reel it in!!");
                            printer.wait(3);
                            printer.print("<bold red>Oh No! It got away and stole your bait!</bold red>");
                            printer.print(baitStolen);

                            player.setHookBaited(false);
                            playerRepository.save(player);
                            printer.print("Try again?");
                            if (prompt.getYesNo()) {
                                load();
                            } else {
                                return;
                            }
                            break;
                        case 5:  //think you hook a fish but it turns out to be a tire or boot or log or line-break
                            ArrayList<String> junk = new ArrayList<>(Arrays.asList("caught a tire", "snagged a log", "caught an old boot", "broke your line"));
                            int junkIndex = random.nextInt(4);
                            printer.print("Woo hoo, I think it's a bite! Set the hook and reel it in!!");
                            printer.wait(3);
                            printer.print("It's so hard to reel in, it must be HUGE!");
                            if (junkIndex != 2)
                                printer.wait(1);
                            printer.print("<bold red>Oh No! You %s!</bold red>", junk.get(junkIndex));
                            printer.print(sadPerson);
                            if (junkIndex == 3) {
                                player.setHookBaited(false);
                                playerRepository.save(player);
                            }
                            printer.print("Try again?");
                            if (prompt.getYesNo()) {
                                load();
                            } else {
                                return;
                            }
                            break;
                        default:

                            printer.print("Still waiting...");
                            printer.wait(3);
                            printer.print(troll);
                            printer.print("Nothing is biting! Try again?");
                            if (prompt.getYesNo()) {
                                load();
                            } else {
                                return;
                            }
                            break;
                    }
                } else {
                    printer.print("%nOops your hook is <bold red>empty</bold red>! You must bait your hook before casting.");
                    load();
                }
                break;
            default:
                break;
        }
    }

    private void baitHook(TackleBox tackleBoxBait) {
        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player = players.get(0);
        Bait removedBait = player.getBait();

        if (tackleBoxBait.getBait().getType().equals(removedBait.getType()) && player.getHookBaited()) {
            printer.print("%nThe bait you selected is already on your hook.");
        } else {
            Iterable<TackleBox> tackleBoxIterable = tackleBoxRepository.findAll();
            List<TackleBox> tackle = Streamable.of(tackleBoxIterable).toList();
            ArrayList<TackleBox> tackleBoxArrayList = new ArrayList<>(tackle);
                printer.print("Are you sure?");
                if (prompt.getYesNo()) {
                    player.setBait(tackleBoxBait.getBait());
                    playerRepository.save(player);

                    boolean baitRemovedFound = false;
                    for (int i = 0; i < tackleBoxArrayList.size(); i++) {
                        TackleBox currentItem = tackleBoxArrayList.get(i);
                        if (currentItem.getBait().getType().equals(removedBait.getType())) {
                            baitRemovedFound = true;
                            if (player.getHookBaited()) {
                                Integer newQuantity = currentItem.getQuantity() + 1;
                                TackleBox newItem = new TackleBox(removedBait, newQuantity, player);
                                tackleBoxRepository.delete(currentItem);
                                tackleBoxRepository.save(newItem);
                            }
                        }
                    }
                    if (baitRemovedFound == false && player.getHookBaited()) {
                        TackleBox returnedItem = new TackleBox(removedBait, 1, player);
                        tackleBoxRepository.save(returnedItem);
                    }
                    for (int j = 0; j < tackleBoxArrayList.size(); j++) {
                        TackleBox itemToRemove = tackleBoxArrayList.get(j);
                        if (itemToRemove.getBait().getType().equals(tackleBoxBait.getBait().getType())) {
                            int newQuantity = itemToRemove.getQuantity() - 1;
                            TackleBox newItem = new TackleBox(tackleBoxBait.getBait(), newQuantity, player);
                            tackleBoxRepository.delete(itemToRemove);
                            if (newQuantity > 0)
                                tackleBoxRepository.save(newItem);
                            player.setHookBaited(true);
                            playerRepository.save(player);
                        }
                    }
                }
            }
        load();
    }

    private void catchFish(List<Fish> fishInLocationsThatLikesBait, Integer rodStrength) {
        Iterable<Player> playersIterable = playerRepository.findAll();
        List<Player> players = Streamable.of(playersIterable).toList();
        Player player = players.get(0);

        Random random = new Random();
        int randomIndex = random.nextInt(fishInLocationsThatLikesBait.size());
        String speciesHooked = fishInLocationsThatLikesBait.get(randomIndex).getSpecies();
        Integer rewardMoney = fishInLocationsThatLikesBait.get(randomIndex).getReward();
        Integer fishStrength = fishInLocationsThatLikesBait.get(randomIndex).getFishStrength();

        printer.print("Woo hoo, I think it's a bite! Set the hook and reel it in!!");
        printer.wait(3);
        if (rodStrength < fishStrength) {
            printer.print("You hooked a monster %s, your fishing rod couldn't handle it! Quick cut your line before your rod breaks!", speciesHooked);
            printer.print("Would you like to try again?");
            player.setHookBaited(false);
            playerRepository.save(player);
        } else {
            Integer currentBalance = player.getBank();
            player.setBank(currentBalance + rewardMoney);
            playerRepository.save(player);
            Integer updatedBalance = player.getBank();
            printer.print("<bold blue>Congratulations!</bold blue> You landed a %s and earned yourself a <green>$%d</green> reward! Your new bank balance is <green>$%d</green>.", speciesHooked, rewardMoney, updatedBalance);
            printer.print(fishInLocationsThatLikesBait.get(randomIndex).getArt());
            printer.print("Would you like to continue fishing?");
            if (player.getBait().isReal()) {
                player.setHookBaited(false);
                playerRepository.save(player);
            }
        }
        if (prompt.getYesNo()) {
            load();
        }
    }
}
