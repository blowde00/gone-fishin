package com.galvanize.gonefishin.data;

public enum SettingType {
  STRING,
  NUMBER,
  BINARY
}
